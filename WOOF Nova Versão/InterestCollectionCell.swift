//
//  InterestCollectionCell.swift
//  WOOF Nova Versão
//
//  Created by Gerson Rodrigo on 16/07/16.
//  Copyright © 2016 Gerson Rodrigo. All rights reserved.
//

import UIKit

class InterestCollectionCell: UICollectionViewCell {
    var interest: Animal!{
        didSet{
            updateUI()
        }
    }
    @IBOutlet weak var fotoAnimal: UIImageView!
    @IBOutlet weak var AniversarioAnimal: UIButton!
    @IBOutlet weak var personalidadeAnimal: UIButton!
    
    @IBOutlet weak var pesoAnimal: UIButton!
    @IBOutlet weak var porteAnimal: UIButton!
    @IBOutlet weak var racaAnimal: UIButton!
    @IBOutlet weak var SexoAnimal: UIButton!
    
    func calculateAge (birthday: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Year, fromDate: birthday, toDate: NSDate(), options: []).year
    }
    private func updateUI(){
        fotoAnimal.image = interest.foto
        AniversarioAnimal.setTitle(String(calculateAge(interest.idade!)), forState: .Normal)
        SexoAnimal.setTitle(interest.sexo, forState: .Normal)
        porteAnimal.setTitle(interest.porte, forState: .Normal)
        personalidadeAnimal.setTitle(interest.personalidade, forState: .Normal)
        pesoAnimal.setTitle(interest.peso, forState: .Normal)
        racaAnimal.setTitle(interest.raca, forState: .Normal)
        if(interest.sexo == "Macho"){
            SexoAnimal.setImage(UIImage(named: "Icon_Outline_macho"), forState: .Normal)
        }else{
            SexoAnimal.setImage(UIImage(named: "Icon_Outline_Femea"), forState: .Normal)
        }
        if(interest.foto == nil){
            fotoAnimal.image = UIImage(named: "logo_Icon_Woof_4")
        }else{
            fotoAnimal.image = interest.foto
        }

    }
    
}
