//
//  AnimalStore.swift
//  WOOF Nova Versão
//
//  Created by Gerson Rodrigo on 16/07/16.
//  Copyright © 2016 Gerson Rodrigo. All rights reserved.
//

import UIKit
import CoreData
class AnimalStore: NSObject {
    static let singgleton = AnimalStore()
    
    
    private var managedObjectContext: NSManagedObjectContext{
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        return appDelegate.managedObjectContext
    }
    
    private override init() {
        super.init()
    }
    
    func creatAnimal()-> Animal{
        return NSEntityDescription.insertNewObjectForEntityForName("Animal", inManagedObjectContext: managedObjectContext) as! Animal
    }
    
    func salvarAnimal(animal: Animal){
        do {
            return try self.managedObjectContext.save()
        }catch{
            print("Erro ao salvar animal: \(error)")
        }
    }
    
    func removerAnimal(animal: Animal){
        self.managedObjectContext.deleteObject(animal)
        do{
            try self.managedObjectContext.save()
        } catch {
            print("Erro ao salvar Animal \(error)")
        }
    }
    
    func getAllAnimais() ->[Animal]{
        let fetchResquest = NSFetchRequest(entityName: "Animal")
        let sortDescriptor = NSSortDescriptor(key: "nome",ascending: true)
        
        fetchResquest.sortDescriptors = [sortDescriptor]
        do {
            return try self.managedObjectContext.executeFetchRequest(fetchResquest) as! [Animal]
        }catch {
            print("Erro ao pegar os salvos: \(error)")
            return[]
        }
    }
    
    func listar()->[Animal]?{
        let contexto = self.managedObjectContext
        let pedido = NSFetchRequest(entityName: "Animal")
        let ordem = NSSortDescriptor(key:  "nome", ascending: true)
        
        pedido.sortDescriptors = [ordem]
        
        do {
            let resultado = try contexto.executeFetchRequest(pedido) as! [Animal]
            return resultado
        }catch{
            print("Error retiever nimal")
        }
        return nil
    }

}
