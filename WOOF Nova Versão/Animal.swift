//
//  Animal.swift
//  WOOF Nova Versão
//
//  Created by Gerson Rodrigo on 16/07/16.
//  Copyright © 2016 Gerson Rodrigo. All rights reserved.
//

import UIKit
import CoreData
class Animal: NSManagedObject {
    @NSManaged var nome: String?
    @NSManaged var idade: NSDate?
    @NSManaged var raca: String?
    @NSManaged var sexo: String?
    @NSManaged var personalidade: String?
    @NSManaged var peso: String?
    @NSManaged var porte: String?
    @NSManaged var foto: UIImage?
}
