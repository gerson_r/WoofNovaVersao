//
//  AdicionarAnimalViewController.swift
//  WOOF Nova Versão
//
//  Created by Gerson Rodrigo on 17/07/16.
//  Copyright © 2016 Gerson Rodrigo. All rights reserved.
//

import UIKit

class AdicionarAnimalViewController: UIViewController,  UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate{

    @IBOutlet weak var Nome: UITextField!
    @IBOutlet weak var Nascimento: UITextField!
    @IBOutlet weak var Raca: UITextField!
    @IBOutlet weak var segmenteSexo: UISegmentedControl!
    @IBOutlet weak var Peso: UITextField!
    @IBOutlet weak var segmentPorte: UISegmentedControl!
    @IBOutlet weak var Personalidade: UITextField!
    @IBOutlet weak var FotoAnimal: UIButton!
    
    var sexo: String?
    var porte: String?
    var imagem: UIImage!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       customizar()
        
        //Criando o delegate dos texts
        Nome.delegate = self
        Nascimento.delegate = self
        Raca.delegate = self
        Peso.delegate = self
        Personalidade.delegate = self
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //Customiza os elemntos da TELA
    func customizar(){
        Nome.text = "Nome"
        Nascimento.text = "Nascimento"
        Raca.text = "Raça"
        Peso.text = "Peso"
        Personalidade.text = "Personalidade"
    }
    
    func dismissKeyboard(){
        Nome.resignFirstResponder()
        Nascimento.resignFirstResponder()
        Raca.resignFirstResponder()
        Peso.resignFirstResponder()
        Personalidade.resignFirstResponder()
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @IBAction func Salvar(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
        print("salvar")
    }

    @IBAction func Cancelar(sender: AnyObject) {
         self.navigationController?.popViewControllerAnimated(true)
        print("canelar")
    }
    
    @IBAction func FotoCachorro(sender: AnyObject) {
        guard let viewRect = sender as? UIView else {
            return
        }
        
        let cameraSettingsAlert = UIAlertController(title: NSLocalizedString("Escolha a opção", comment: ""), message: NSLocalizedString("", comment: ""), preferredStyle: .ActionSheet)
        cameraSettingsAlert.modalPresentationStyle = .Popover
        
        let tirarfoto = UIAlertAction(title: NSLocalizedString("Tirar Foto", comment: ""), style: .Default) { action in
            let picker = UIImagePickerController()
            
            picker.delegate = self
            picker.sourceType = .Camera
            picker.allowsEditing = true
            self.presentViewController(picker, animated: true, completion: nil)
            print("Tirar Foto")
        }
        let galeria = UIAlertAction(title: NSLocalizedString("Galeria de Fotos", comment: ""), style: .Default) { action in
            
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = .PhotoLibrary
            picker.allowsEditing = true
            self.presentViewController(picker, animated: true, completion: nil)
            print("Galeria de Fotos")
        }
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .Cancel) { action in
            
        }
        
        cameraSettingsAlert.addAction(galeria)
        cameraSettingsAlert.addAction(tirarfoto)
        cameraSettingsAlert.addAction(cancel)
        
        if let presenter = cameraSettingsAlert.popoverPresentationController {
            presenter.sourceView = viewRect;
            presenter.sourceRect = viewRect.bounds;
        }
        presentViewController(cameraSettingsAlert, animated: true, completion: nil)
        
        
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        
        imagem = (info[UIImagePickerControllerEditedImage] as? UIImage)!
        FotoAnimal.layer.cornerRadius = FotoAnimal.frame.size.width/2
        FotoAnimal.clipsToBounds = true
        FotoAnimal.setImage(imagem, forState: .Normal)
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    @IBAction func sexoControl(sender: AnyObject) {
        switch segmenteSexo.selectedSegmentIndex {
        case 0:
            sexo = "Macho"
        case 1:
            sexo = "Fêmea"
        default:
            break
        }
    }

    @IBAction func porteControl(sender: AnyObject) {
        switch  segmentPorte.selectedSegmentIndex {
        case 0:
            porte = "Pequeno"
        case 1:
            porte = "Médio"
        case 2:
            porte = "Grande"
        default:
            break
        }
    }

}
