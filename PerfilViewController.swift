//
//  PerfilViewController.swift
//  WOOF Nova Versão
//
//  Created by Gerson Rodrigo on 16/07/16.
//  Copyright © 2016 Gerson Rodrigo. All rights reserved.
//

import UIKit

class PerfilViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
  //  private var interests = Interest.createInterests()
    var animais: [Animal]!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    private struct Storyboard{
        static let CellIdentifier = "Interest Cell"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    
    }
}
    extension PerfilViewController: UICollectionViewDataSource{
        func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
            return 1
            
        }
        func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           if animais == nil{
                return 0
           }else{
            return animais.count
            }
        
        }
        
        func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(Storyboard.CellIdentifier, forIndexPath: indexPath) as! InterestCollectionCell
            
            cell.interest = self.animais[indexPath.item]
            
            return cell
        }
    }
